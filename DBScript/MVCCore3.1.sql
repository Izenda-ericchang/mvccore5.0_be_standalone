USE [master]
GO
/****** Object:  Database [MVCCore3.1]    Script Date: 11/4/2020 9:41:20 AM ******/
CREATE DATABASE [MVCCore3.1]

GO
ALTER DATABASE [MVCCore3.1] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [MVCCore3.1].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [MVCCore3.1] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [MVCCore3.1] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [MVCCore3.1] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [MVCCore3.1] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [MVCCore3.1] SET ARITHABORT OFF 
GO
ALTER DATABASE [MVCCore3.1] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [MVCCore3.1] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [MVCCore3.1] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [MVCCore3.1] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [MVCCore3.1] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [MVCCore3.1] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [MVCCore3.1] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [MVCCore3.1] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [MVCCore3.1] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [MVCCore3.1] SET  ENABLE_BROKER 
GO
ALTER DATABASE [MVCCore3.1] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [MVCCore3.1] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [MVCCore3.1] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [MVCCore3.1] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [MVCCore3.1] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [MVCCore3.1] SET READ_COMMITTED_SNAPSHOT ON 
GO
ALTER DATABASE [MVCCore3.1] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [MVCCore3.1] SET RECOVERY FULL 
GO
ALTER DATABASE [MVCCore3.1] SET  MULTI_USER 
GO
ALTER DATABASE [MVCCore3.1] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [MVCCore3.1] SET DB_CHAINING OFF 
GO
ALTER DATABASE [MVCCore3.1] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [MVCCore3.1] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [MVCCore3.1] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'MVCCore3.1', N'ON'
GO
ALTER DATABASE [MVCCore3.1] SET QUERY_STORE = OFF
GO
USE [MVCCore3.1]
GO
/****** Object:  Table [dbo].[__EFMigrationsHistory]    Script Date: 11/4/2020 9:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[__EFMigrationsHistory](
	[MigrationId] [nvarchar](150) NOT NULL,
	[ProductVersion] [nvarchar](32) NOT NULL,
 CONSTRAINT [PK___EFMigrationsHistory] PRIMARY KEY CLUSTERED 
(
	[MigrationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoleClaims]    Script Date: 11/4/2020 9:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoleClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoleClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetRoles]    Script Date: 11/4/2020 9:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetRoles](
	[Id] [nvarchar](450) NOT NULL,
	[Name] [nvarchar](256) NULL,
	[NormalizedName] [nvarchar](256) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetRoles] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserClaims]    Script Date: 11/4/2020 9:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserClaims](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[UserId] [nvarchar](450) NOT NULL,
	[ClaimType] [nvarchar](max) NULL,
	[ClaimValue] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserClaims] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserLogins]    Script Date: 11/4/2020 9:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserLogins](
	[LoginProvider] [nvarchar](128) NOT NULL,
	[ProviderKey] [nvarchar](128) NOT NULL,
	[ProviderDisplayName] [nvarchar](max) NULL,
	[UserId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserLogins] PRIMARY KEY CLUSTERED 
(
	[LoginProvider] ASC,
	[ProviderKey] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserRoles]    Script Date: 11/4/2020 9:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserRoles](
	[UserId] [nvarchar](450) NOT NULL,
	[RoleId] [nvarchar](450) NOT NULL,
 CONSTRAINT [PK_AspNetUserRoles] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUsers]    Script Date: 11/4/2020 9:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUsers](
	[Id] [nvarchar](450) NOT NULL,
	[UserName] [nvarchar](256) NULL,
	[NormalizedUserName] [nvarchar](256) NULL,
	[Email] [nvarchar](256) NULL,
	[NormalizedEmail] [nvarchar](256) NULL,
	[EmailConfirmed] [bit] NOT NULL,
	[PasswordHash] [nvarchar](max) NULL,
	[SecurityStamp] [nvarchar](max) NULL,
	[ConcurrencyStamp] [nvarchar](max) NULL,
	[PhoneNumber] [nvarchar](max) NULL,
	[PhoneNumberConfirmed] [bit] NOT NULL,
	[TwoFactorEnabled] [bit] NOT NULL,
	[LockoutEnd] [datetimeoffset](7) NULL,
	[LockoutEnabled] [bit] NOT NULL,
	[AccessFailedCount] [int] NOT NULL,
	[TenantId] [int] NULL,
	[Discriminator] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_AspNetUsers] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[AspNetUserTokens]    Script Date: 11/4/2020 9:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AspNetUserTokens](
	[UserId] [nvarchar](450) NOT NULL,
	[LoginProvider] [nvarchar](128) NOT NULL,
	[Name] [nvarchar](128) NOT NULL,
	[Value] [nvarchar](max) NULL,
 CONSTRAINT [PK_AspNetUserTokens] PRIMARY KEY CLUSTERED 
(
	[UserId] ASC,
	[LoginProvider] ASC,
	[Name] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Tenants]    Script Date: 11/4/2020 9:41:20 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Tenants](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NOT NULL,
 CONSTRAINT [PK_Tenants] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200924151151_AddDefaultIdentityMigration', N'3.1.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200924171402_AddTenantToDb', N'3.1.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20200928201236_UpdateTenantName', N'3.1.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201001164954_addTenantIdPropertyToDb', N'3.1.8')
INSERT [dbo].[__EFMigrationsHistory] ([MigrationId], [ProductVersion]) VALUES (N'20201001180444_updateUserTenantIdNullable', N'3.1.8')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'017402b4-ae46-43ca-a6c2-8a996680179b', N'VP', N'VP', N'84f1c5cf-275c-462a-9e09-23c1f62e4a0d')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'ce0ab481-07d4-46ac-b6a4-c6ccf5f2792a', N'Employee', N'EMPLOYEE', N'2a7f63b7-9e5d-458a-8088-16876e8d87a0')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'f49f3238-c6c0-4c71-91e3-71fe71c3d51a', N'Admin', N'ADMIN', N'190715fb-137f-4135-ae45-ff69d8f8a863')
INSERT [dbo].[AspNetRoles] ([Id], [Name], [NormalizedName], [ConcurrencyStamp]) VALUES (N'ff8ef4cf-f917-4da4-85a4-776385028670', N'Manager', N'MANAGER', N'2eec5da6-1021-4c2b-a941-3830d7424b2e')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'1709c974-6067-43db-b5ff-63d0d2d7d87b', N'017402b4-ae46-43ca-a6c2-8a996680179b')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'95e3336e-4553-44cb-b214-641525a308e9', N'017402b4-ae46-43ca-a6c2-8a996680179b')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'eda4c5a3-7135-49a0-8bc5-709f6a929cbc', N'017402b4-ae46-43ca-a6c2-8a996680179b')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'4b43d818-3b9a-4b4d-b5a7-d2dac530b28f', N'ce0ab481-07d4-46ac-b6a4-c6ccf5f2792a')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'b64513ce-29a5-415a-ba55-4357050d61d9', N'ce0ab481-07d4-46ac-b6a4-c6ccf5f2792a')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c742c74f-5c03-4ae4-871f-70154e8d7de0', N'ce0ab481-07d4-46ac-b6a4-c6ccf5f2792a')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'da30e4db-37da-491e-82e7-88ea5d8b2bf0', N'f49f3238-c6c0-4c71-91e3-71fe71c3d51a')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'2c253e56-6e26-4c94-9028-967bc6231f85', N'ff8ef4cf-f917-4da4-85a4-776385028670')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'bb951be1-ec5f-4da8-a860-cb2ecf522cdd', N'ff8ef4cf-f917-4da4-85a4-776385028670')
INSERT [dbo].[AspNetUserRoles] ([UserId], [RoleId]) VALUES (N'c92a4cdc-09c5-4cdd-a8d5-1271f647c99e', N'ff8ef4cf-f917-4da4-85a4-776385028670')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [TenantId], [Discriminator]) VALUES (N'1709c974-6067-43db-b5ff-63d0d2d7d87b', N'vp@retcl.com', N'VP@RETCL.COM', N'vp@retcl.com', N'VP@RETCL.COM', 0, N'AQAAAAEAACcQAAAAEE+LgPI4rVctV4fGlp2l9iNAt6JIQhRfJLpndwCWSxesf5cw8xuKmsGcLZrcMFmVsw==', N'KCDZ5JMZLFNQZDKZTH6YSAC7NFYTQ67A', N'8ef8ecdf-22f8-4142-81c1-99e75713929d', NULL, 0, 0, NULL, 1, 0, 3, N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [TenantId], [Discriminator]) VALUES (N'2c253e56-6e26-4c94-9028-967bc6231f85', N'manager@natwr.com', N'MANAGER@NATWR.COM', N'manager@natwr.com', N'MANAGER@NATWR.COM', 0, N'AQAAAAEAACcQAAAAEFNvAlrubH/MYl6nh4I18n9DPArcQJAUFIhQHUNPtJhQKRLbd+zEUSwy5sE0Wph2aA==', N'MMRSDIDJVDN2M4I27HZQ2XNBYTIS2KRR', N'23843268-5e9f-422f-84e3-fc387c37ee12', NULL, 0, 0, NULL, 1, 0, 2, N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [TenantId], [Discriminator]) VALUES (N'4b43d818-3b9a-4b4d-b5a7-d2dac530b28f', N'employee@retcl.com', N'EMPLOYEE@RETCL.COM', N'employee@retcl.com', N'EMPLOYEE@RETCL.COM', 0, N'AQAAAAEAACcQAAAAEPduqzuLed8K5qCihtm7rSYSdWBJ/ZE1QM58b7zuoDuBdSM1Ph+YG2yfkCdXm5lzaA==', N'K6A23KJ3VDARWTL7W5ZZI7CHFCM76NE5', N'83eabc25-9ea7-4e79-9ce3-f4222811370a', NULL, 0, 0, NULL, 1, 0, 3, N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [TenantId], [Discriminator]) VALUES (N'95e3336e-4553-44cb-b214-641525a308e9', N'vp@natwr.com', N'VP@NATWR.COM', N'vp@natwr.com', N'VP@NATWR.COM', 0, N'AQAAAAEAACcQAAAAEMG5URoGhDs5OuHIGxsUum2fGgj5HO+2BqABsPoHF5+2r6LHWWG9vOCwtbo1PAc1Vg==', N'OPMSHYRK7PKMFXNQU4CMGET5YECEDKGK', N'c7f52cdf-11f7-42bd-9d15-70141118eed3', NULL, 0, 0, NULL, 1, 0, 2, N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [TenantId], [Discriminator]) VALUES (N'b64513ce-29a5-415a-ba55-4357050d61d9', N'employee@natwr.com', N'EMPLOYEE@NATWR.COM', N'employee@natwr.com', N'EMPLOYEE@NATWR.COM', 0, N'AQAAAAEAACcQAAAAEO8AZtUGB3y0UPduVjWSAZHT1RlwLg3ZY40eqEZXRSlYaoDIFOkfDtYY4iDcYE1OGQ==', N'EERVFWFRMPXALXB7GC4VPPJB4L74FQ34', N'd895e3f7-d17f-446a-9a30-2ca173d9f1b2', NULL, 0, 0, NULL, 1, 0, 2, N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [TenantId], [Discriminator]) VALUES (N'bb951be1-ec5f-4da8-a860-cb2ecf522cdd', N'manager@deldg.com', N'MANAGER@DELDG.COM', N'manager@deldg.com', N'MANAGER@DELDG.COM', 0, N'AQAAAAEAACcQAAAAEE49wKGgYaBg04AhplXCY8JEy/fkupG0McMKe9PldR8FHso/kkHolpTB9P2idioRYg==', N'GSJB6EJXGAEETSEM4GAXK5YOYHIQLPDC', N'c4185c4c-5396-48c4-9422-29f64023023d', NULL, 0, 0, NULL, 1, 0, 1, N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [TenantId], [Discriminator]) VALUES (N'c742c74f-5c03-4ae4-871f-70154e8d7de0', N'employee@deldg.com', N'EMPLOYEE@DELDG.COM', N'employee@deldg.com', N'EMPLOYEE@DELDG.COM', 0, N'AQAAAAEAACcQAAAAEAMkb+3wBP7INC27C/cAyhahz+NwTos1KI4XydTtiHmk041s+TrzQuWiYZ2bGxSj7w==', N'BIOD4EKF2BTWG6NACLHBAN4XARNMCYE4', N'44561c79-8c8c-4a86-916d-23b7c46a948d', NULL, 0, 0, NULL, 1, 0, 1, N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [TenantId], [Discriminator]) VALUES (N'c92a4cdc-09c5-4cdd-a8d5-1271f647c99e', N'manager@retcl.com', N'MANAGER@RETCL.COM', N'manager@retcl.com', N'MANAGER@RETCL.COM', 0, N'AQAAAAEAACcQAAAAEGkSrAvqxqyNlgDO2WGz8MTxAG7fodxuF8FXsUMcNuyCkHZtvF6ZmAMvCS3BaPCFrA==', N'D54WQEHQB3UVQV35NGS745Y4APYYAUZL', N'f02b5ad8-b103-48f9-bc6d-4cfe13cd947d', NULL, 0, 0, NULL, 1, 0, 3, N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [TenantId], [Discriminator]) VALUES (N'da30e4db-37da-491e-82e7-88ea5d8b2bf0', N'IzendaAdmin@system.com', N'IZENDAADMIN@SYSTEM.COM', N'IzendaAdmin@system.com', N'IZENDAADMIN@SYSTEM.COM', 0, N'AQAAAAEAACcQAAAAEOhorPlg7cPlJDzjBgNGKQI4oNJOKEQfOA1PpLDQdpXkBkFVE/4FVEN0nRKazMQwdw==', N'KZFHEO5R7SZTDLSLEVJXKNZI6NC5EADT', N'7c6c8c60-93ae-470d-aa76-208a3b47a1c9', NULL, 0, 0, NULL, 1, 0, NULL, N'ApplicationUser')
INSERT [dbo].[AspNetUsers] ([Id], [UserName], [NormalizedUserName], [Email], [NormalizedEmail], [EmailConfirmed], [PasswordHash], [SecurityStamp], [ConcurrencyStamp], [PhoneNumber], [PhoneNumberConfirmed], [TwoFactorEnabled], [LockoutEnd], [LockoutEnabled], [AccessFailedCount], [TenantId], [Discriminator]) VALUES (N'eda4c5a3-7135-49a0-8bc5-709f6a929cbc', N'vp@deldg.com', N'VP@DELDG.COM', N'vp@deldg.com', N'VP@DELDG.COM', 0, N'AQAAAAEAACcQAAAAEOPk4iepBlC/o3YNknE0tmQmAbTAUdJsdX0hFhngLvW19J8OTch0ou8BZn5Ez8w/2w==', N'M6IYDFCUHCRY5N3PZFAA3WP5Z6SPGAXA', N'0acee98e-a0fb-4541-9400-cd47559e41eb', NULL, 0, 0, NULL, 1, 0, 1, N'ApplicationUser')
SET IDENTITY_INSERT [dbo].[Tenants] ON 

INSERT [dbo].[Tenants] ([Id], [Name]) VALUES (1, N'DELDG')
INSERT [dbo].[Tenants] ([Id], [Name]) VALUES (2, N'NATWR')
INSERT [dbo].[Tenants] ([Id], [Name]) VALUES (3, N'RETCL')
SET IDENTITY_INSERT [dbo].[Tenants] OFF
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetRoleClaims_RoleId]    Script Date: 11/4/2020 9:41:20 AM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetRoleClaims_RoleId] ON [dbo].[AspNetRoleClaims]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [RoleNameIndex]    Script Date: 11/4/2020 9:41:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [RoleNameIndex] ON [dbo].[AspNetRoles]
(
	[NormalizedName] ASC
)
WHERE ([NormalizedName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserClaims_UserId]    Script Date: 11/4/2020 9:41:20 AM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserClaims_UserId] ON [dbo].[AspNetUserClaims]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserLogins_UserId]    Script Date: 11/4/2020 9:41:20 AM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserLogins_UserId] ON [dbo].[AspNetUserLogins]
(
	[UserId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [IX_AspNetUserRoles_RoleId]    Script Date: 11/4/2020 9:41:20 AM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUserRoles_RoleId] ON [dbo].[AspNetUserRoles]
(
	[RoleId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [EmailIndex]    Script Date: 11/4/2020 9:41:20 AM ******/
CREATE NONCLUSTERED INDEX [EmailIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedEmail] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
/****** Object:  Index [IX_AspNetUsers_Tenant_Id]    Script Date: 11/4/2020 9:41:20 AM ******/
CREATE NONCLUSTERED INDEX [IX_AspNetUsers_Tenant_Id] ON [dbo].[AspNetUsers]
(
	[TenantId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
SET ANSI_PADDING ON
GO
/****** Object:  Index [UserNameIndex]    Script Date: 11/4/2020 9:41:20 AM ******/
CREATE UNIQUE NONCLUSTERED INDEX [UserNameIndex] ON [dbo].[AspNetUsers]
(
	[NormalizedUserName] ASC
)
WHERE ([NormalizedUserName] IS NOT NULL)
WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
GO
ALTER TABLE [dbo].[AspNetUsers] ADD  DEFAULT (N'') FOR [Discriminator]
GO
ALTER TABLE [dbo].[AspNetRoleClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetRoleClaims] CHECK CONSTRAINT [FK_AspNetRoleClaims_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserClaims]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserClaims] CHECK CONSTRAINT [FK_AspNetUserClaims_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserLogins]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserLogins] CHECK CONSTRAINT [FK_AspNetUserLogins_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId] FOREIGN KEY([RoleId])
REFERENCES [dbo].[AspNetRoles] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetRoles_RoleId]
GO
ALTER TABLE [dbo].[AspNetUserRoles]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserRoles] CHECK CONSTRAINT [FK_AspNetUserRoles_AspNetUsers_UserId]
GO
ALTER TABLE [dbo].[AspNetUsers]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUsers_Tenants_Tenant_Id] FOREIGN KEY([TenantId])
REFERENCES [dbo].[Tenants] ([Id])
GO
ALTER TABLE [dbo].[AspNetUsers] CHECK CONSTRAINT [FK_AspNetUsers_Tenants_Tenant_Id]
GO
ALTER TABLE [dbo].[AspNetUserTokens]  WITH CHECK ADD  CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId] FOREIGN KEY([UserId])
REFERENCES [dbo].[AspNetUsers] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[AspNetUserTokens] CHECK CONSTRAINT [FK_AspNetUserTokens_AspNetUsers_UserId]
GO
USE [master]
GO
ALTER DATABASE [MVCCore3.1] SET  READ_WRITE 
GO
