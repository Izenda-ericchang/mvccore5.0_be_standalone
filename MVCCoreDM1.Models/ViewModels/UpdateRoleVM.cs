﻿using System.ComponentModel.DataAnnotations;

namespace MVCCoreDM1.Models.ViewModels
{
    public class UpdateRoleVM
    {
        #region Properties
        [Display(Name = "Role Name")]
        [Required]
        public string RoleName { get; set; }

        [Display(Name = "Tenant Name")]
        [Required]
        public string TenantDetailName { get; set; }

        [Display(Name = "New Role Name")]
        [Required]
        public string NewRoleName { get; set; }
        #endregion
    } 
}
