﻿using System.ComponentModel.DataAnnotations;

namespace MVCCoreDM1.Models.ViewModels
{
    public class TenantVM
    {
        #region Properties
        public Tenant Tenant { get; set; }

        [Display(Name = "Tenant Name")]
        [Required]
        public string TenantDetailName { get; set; } 
        #endregion
    }
}
