﻿using System;
using System.Collections.Generic;

namespace MVCCoreDM1.Models.IzendaModel
{
    public class RoleDetail : RoleInfo
    {
        #region Properties
        public bool Active { get; set; }

        public bool Deleted { get; set; }

        public Guid? TenantId { get; set; }

        public bool NotAllowSharing { get; set; }

        public int State { get; set; }

        public int Version { get; set; }

        public DateTime? Modified { get; set; }

        public bool IsDirty { get; set; }

        public string ModifiedBy { get; set; }

        public string TenantUniqueName { get; set; }

        public Permission Permission { get; set; }
        #endregion
    }
}
