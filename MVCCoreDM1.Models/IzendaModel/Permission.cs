﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MVCCoreDM1.Models.IzendaModel
{
    public class Permission
    {
        #region Properties
        public bool FullReportAndDashboardAccess { get; set; } 
        #endregion
    }
}
