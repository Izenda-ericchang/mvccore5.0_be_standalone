﻿using System;

namespace MVCCoreDM1.Models.IzendaModel
{
    public class RoleInfo
    {
        #region Properties
        public Guid Id { get; set; }

        public string Name { get; set; }
        #endregion
    }
}
