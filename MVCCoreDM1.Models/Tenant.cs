﻿using System.ComponentModel.DataAnnotations;

namespace MVCCoreDM1.Models
{
    public class Tenant
    {
        #region Properties
        [Key]
        public int Id { get; set; }

        [Display(Name = "Tenant ID")]
        [Required]
        public string Name { get; set; } 
        #endregion
    }
}
