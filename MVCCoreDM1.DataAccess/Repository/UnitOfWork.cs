﻿using MVCCoreDM1.DataAccess.Data;
using MVCCoreDM1.DataAccess.Repository.IRepository;
using System.Threading.Tasks;

namespace MVCCoreDM1.DataAccess.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        #region Variables
        private readonly ApplicationDbContext _db; 
        #endregion

        #region Properties
        public ITenantRepository Tenant { get; private set; }

        public IApplicationUserRepository ApplicationUser { get; private set; }

        public ISP_Call SP_Call { get; private set; } 
        #endregion

        #region CTOR
        public UnitOfWork(ApplicationDbContext db)
        {
            _db = db;

            Tenant = new TenantRepository(_db);
            ApplicationUser = new ApplicationUserRepository(_db);
            SP_Call = new SP_Call(_db);
        } 
        #endregion

        #region Methods
        public void Dispose()
        {
            _db.Dispose();
        }

        public void Save()
        {
            _db.SaveChanges();
        }

        public Task SaveAsync()
        {
            return _db.SaveChangesAsync();
        }
        #endregion
    }
}
