﻿using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using MVCCoreDM1.DataAccess.Data;
using MVCCoreDM1.DataAccess.Repository.IRepository;
using MVCCoreDM1.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MVCCoreDM1.DataAccess.Repository
{
    public class ApplicationUserRepository : Repository<ApplicationUser>, IApplicationUserRepository
    {
        #region Variables
        private readonly ApplicationDbContext _db;
        #endregion

        #region CTOR
        public ApplicationUserRepository(ApplicationDbContext db) : base(db)
        {
            _db = db;
        }
        #endregion

        #region Methods
        public async Task<ApplicationUser> FindTenantUserAsync(string tenant, string username)
        {
            ApplicationUser user = null;

            if (!string.IsNullOrWhiteSpace(tenant))
            {
                user = await _db.ApplicationUsers
                    .Include(u => u.Tenant)
                    .Where(u => u.UserName.Equals(username))
                    .Where(u => u.Tenant.Name.Equals(tenant))
                    .SingleOrDefaultAsync();
            }
            else
            {
                user = await _db.ApplicationUsers
                    .Include(u => u.Tenant)
                    .Where(u => u.UserName.Equals(username))
                    .Where(u => !u.TenantId.HasValue)
                    .SingleOrDefaultAsync();
            }

            return user;
        }
        #endregion
    }
}
