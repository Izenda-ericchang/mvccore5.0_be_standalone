﻿using MVCCoreDM1.Models;
using System.Threading.Tasks;

namespace MVCCoreDM1.DataAccess.Repository.IRepository
{
    public interface IApplicationUserRepository :IRepository<ApplicationUser>
    {
        #region Methods
        Task<ApplicationUser> FindTenantUserAsync(string tenant, string username); 
        #endregion
    }
}
