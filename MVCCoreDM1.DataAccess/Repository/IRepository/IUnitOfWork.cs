﻿using System;
using System.Threading.Tasks;

namespace MVCCoreDM1.DataAccess.Repository.IRepository
{
    public interface IUnitOfWork : IDisposable
    {
        #region Properties
        ITenantRepository Tenant { get; }

        IApplicationUserRepository ApplicationUser { get; }

        ISP_Call SP_Call { get; }
        #endregion

        #region Methods
        void Save();

        Task SaveAsync();
        #endregion
    }
}
