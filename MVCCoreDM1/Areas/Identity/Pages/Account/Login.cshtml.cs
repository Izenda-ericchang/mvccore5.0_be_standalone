﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using MVCCoreDM1.DataAccess.Repository.IRepository;
using MVCCoreDM1.Models;
using System;
using System.ComponentModel.DataAnnotations;
using System.DirectoryServices.AccountManagement;
using System.IO;
using System.Threading.Tasks;

namespace MVCCoreDM1.Areas.Identity.Pages.Account
{
    using NetCoreSignInResult = Microsoft.AspNetCore.Identity.SignInResult;
    [AllowAnonymous]
    public class LoginModel : PageModel
    {
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly SignInManager<ApplicationUser> _signInManager;
        private readonly ILogger<LoginModel> _logger;
        private readonly IUnitOfWork _unitOfWork;

        public LoginModel(SignInManager<ApplicationUser> signInManager,
            ILogger<LoginModel> logger,
            UserManager<ApplicationUser> userManager,
            IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        [BindProperty]
        public InputModel Input { get; set; }

        public string ReturnUrl { get; set; }

        [TempData]
        public string ErrorMessage { get; set; }

        public class InputModel
        {
            // Tenant is not "[Required]". Null value is allowed
            [DataType(DataType.Text)]
            [Display(Name = "Tenant", Prompt = "System level login does not require tenant field input")]
            public string Tenant { get; set; }

            [Required]
            [EmailAddress]
            public string Email { get; set; }

            [Required]
            [DataType(DataType.Password)]
            public string Password { get; set; }

            [Display(Name = "Remember me?")]
            public bool RememberMe { get; set; }
        }

        public async Task OnGetAsync(string returnUrl = null)
        {
            if (!string.IsNullOrEmpty(ErrorMessage))
            {
                ModelState.AddModelError(string.Empty, ErrorMessage);
            }

            returnUrl = returnUrl ?? Url.Content("~/");

            // Clear the existing external cookie to ensure a clean login process
            await HttpContext.SignOutAsync(IdentityConstants.ExternalScheme);

            ReturnUrl = returnUrl;
        }

        public async Task<IActionResult> OnPostAsync(string returnUrl = null)
        {
            returnUrl = returnUrl ?? Url.Content("~/");

            if (ModelState.IsValid)
            {
                IConfigurationRoot configuration = new ConfigurationBuilder().SetBasePath(Directory.GetCurrentDirectory()).AddJsonFile("appsettings.json").Build();
                bool.TryParse(configuration.GetValue<string>("AppSettings:Settings:useADlogin"), out bool useADlogin);

                // Active Directory login
                if (useADlogin && !string.IsNullOrEmpty(Input.Tenant)) // if tenant is null, then assume that it is system level login. Go to the ValidateLogin which is used for regular login process first
                {
                    bool adSignResult = ValidateActiveDirectoryAccount(Input.Password);

                    if (adSignResult)
                    {
                        // proceed the next step
                        var tenantVerification = await ValidateTenant();

                        if (tenantVerification.Succeeded)
                        {
                            var result = await _signInManager.PasswordSignInAsync(Input.Email, Input.Password, Input.RememberMe, lockoutOnFailure: false);
                            if (result.Succeeded)
                            {
                                _logger.LogInformation("User logged in.");
                                return LocalRedirect(returnUrl);
                            }
                        }
                    }
                }
                else // standard login
                {
                    var tenantVerification = await ValidateTenant();

                    if (tenantVerification.Succeeded)
                    {
                        // This doesn't count login failures towards account lockout
                        // To enable password failures to trigger account lockout, set lockoutOnFailure: true
                        var result = await _signInManager.PasswordSignInAsync(Input.Email, Input.Password, Input.RememberMe, lockoutOnFailure: false);
                        if (result.Succeeded)
                        {
                            _logger.LogInformation("User logged in.");
                            return LocalRedirect(returnUrl);
                        }
                        // Optional
                        return UseTwoFactorAndLockedOutOption(returnUrl, result);
                    }
                }
            }

            // If we got this far, something failed, redisplay form
            ModelState.AddModelError(string.Empty, "Failed to login. Please check your credential.");
            return Page();
        }

        private async Task<NetCoreSignInResult> ValidateTenant()
        {
            var appUser = await _unitOfWork.ApplicationUser.FindTenantUserAsync(Input.Tenant, Input.Email);

            if (appUser != null)
            {
                var userById = await _unitOfWork.ApplicationUser.GetFirstOrDefaultAsync(u => u.Id == appUser.Id);

                var tenantVerification = await _signInManager.CheckPasswordSignInAsync(userById, Input.Password, lockoutOnFailure: false);
                return tenantVerification;
            }
            else
                return NetCoreSignInResult.Failed;
        }

        private bool ValidateActiveDirectoryAccount(string password)
        {
            // Implement this if you want to use AD login

            //var userName = Environment.UserName;
            //var userDomainName = Environment.UserDomainName;
            //var authenticationType = ContextType.Domain;
            //UserPrincipal userPrincipal = null;
            //bool isAuthenticated = false;

            //if (!string.IsNullOrEmpty(userName) && !string.IsNullOrEmpty(userDomainName))
            //{
            //    using (var context = new PrincipalContext(authenticationType, Environment.UserDomainName))
            //    {
            //        try
            //        {
            //            userPrincipal = UserPrincipal.FindByIdentity(context, IdentityType.SamAccountName, userName);

            //            if (userPrincipal != null)
            //            {
            //                isAuthenticated = context.ValidateCredentials(userName, password, ContextOptions.Negotiate);

            //                return isAuthenticated;
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            Console.WriteLine(ex);
            //            return false;
            //        }

            //        if (!isAuthenticated || userPrincipal.IsAccountLockedOut() || (userPrincipal.Enabled.HasValue && !userPrincipal.Enabled.Value))
            //            return false;
            //    }
            //}

            return false;
        }

        private IActionResult UseTwoFactorAndLockedOutOption(string returnUrl, Microsoft.AspNetCore.Identity.SignInResult result)
        {
            if (result.RequiresTwoFactor)
            {
                return RedirectToPage("./LoginWith2fa", new { ReturnUrl = returnUrl, RememberMe = Input.RememberMe });
            }
            if (result.IsLockedOut)
            {
                _logger.LogWarning("User account locked out.");
                return RedirectToPage("./Lockout");
            }

            else
            {
                ModelState.AddModelError(string.Empty, "Invalid login attempt.");
                return Page();
            }
        }
    }
}
