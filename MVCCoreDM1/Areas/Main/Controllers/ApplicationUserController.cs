﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using MVCCoreDM1.DataAccess.Repository.IRepository;
using MVCCoreDM1.IzendaBoundary;
using MVCCoreDM1.Models;
using MVCCoreDM1.Models.ViewModels;
using MVCCoreDM1.Utility;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace MVCCoreDM1.Areas.Main.Controllers
{
    [Area("Main")]
    [ApiExplorerSettings(IgnoreApi = true)]
    [Authorize(Roles = SD.Role_Admin)]
    public class ApplicationUserController : Controller
    {
        #region Variables
        private readonly IUnitOfWork _unitOfWork;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        #endregion

        #region CTOR
        public ApplicationUserController(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole> roleManager, IUnitOfWork unitOfWork)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _unitOfWork = unitOfWork;
        }
        #endregion

        #region Methods
        // GET: Main/ApplicationUser
        public async Task<IActionResult> Index()
        {
            var applicationUserList = await _unitOfWork.ApplicationUser.GetAllAsync(includeProperties: "Tenant");

            return View(applicationUserList);
        }

        // GET: Main/ApplicationUser/Create
        public IActionResult Create()
        {
            ApplicationUserVM applicationUserVM = new ApplicationUserVM()
            {
                TenantList = _unitOfWork.Tenant.GetAll().Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.Id.ToString()
                })
            };

            return View(applicationUserVM);
        }

        // POST: Main/ApplicationUser/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(ApplicationUserVM applicationUserVM)
        {
            if (ModelState.IsValid)
            {
                int? tenantId = null;

                if (applicationUserVM.SelectedTenantId != null)
                {
                    tenantId = applicationUserVM.SelectedTenantId;

                    applicationUserVM.IsAdmin = false;
                }

                var user = new ApplicationUser
                {
                    UserName = applicationUserVM.UserID,
                    Email = applicationUserVM.UserID,
                    Role = applicationUserVM.SelectedRole,
                    TenantId = applicationUserVM.SelectedTenantId
                };

                var result = await _userManager.CreateAsync(user); // Save new user into client DB

                if (result.Succeeded) // if successful, then start creating a user at Izenda DB
                {
                    var assignedRole = !string.IsNullOrEmpty(applicationUserVM.SelectedRole) ? applicationUserVM.SelectedRole : "Employee"; // set default role if required. As an example, Employee is set by default

                    var existingRole = await _roleManager.FindByNameAsync(assignedRole); // check assigned role exist in client DB.

                    if (existingRole == null)
                    {
                        try
                        {
                            await _roleManager.CreateAsync(new IdentityRole(assignedRole));
                            result = await _userManager.AddToRoleAsync(user, assignedRole);
                        }
                        catch (Exception e)
                        {
                            throw new Exception(e.Message);
                        }
                    }
                    else
                    {
                        result = await _userManager.AddToRoleAsync(user, assignedRole);
                    }

                    if (result.Succeeded)
                    {
                        var izendaAdminAuthToken = IzendaTokenAuthorization.GetIzendaAdminToken();
                        //  user.Tenant = _tenantManager.GetTenantById(Input.SelectedTenantId); // set client DB application user's tenant
                        user.Tenant = _unitOfWork.Tenant.GetFirstOrDefault(t => t.Id == applicationUserVM.SelectedTenantId);
                        var tenantName = user.Tenant?.Name ?? null;

                        // Create a new user at Izenda DB
                        var success = await IzendaUtilities.CreateIzendaUser(
                            tenantName,
                            applicationUserVM.UserID,
                            applicationUserVM.LastName,
                            applicationUserVM.FirstName,
                            applicationUserVM.IsAdmin,
                            assignedRole,
                            izendaAdminAuthToken);

                        if (success)
                            return LocalRedirect("/Main/ApplicationUser");
                    }
                    ModelState.AddModelError(string.Empty, "Failed to create a new user. User already exists in DB.");
                }
                else
                    ModelState.AddModelError(string.Empty, "Failed to create a new user. User already exists in DB.");
            }
            return View(new ApplicationUserVM()
            {
                TenantList = _unitOfWork.Tenant.GetAll().Select(i => new SelectListItem
                {
                    Text = i.Name,
                    Value = i.Id.ToString()
                })
            });
        }

        // GET: Dynamic retrieval of roles based on selected tenant
        [HttpGet("GetAllRoles")]
        public async Task<IActionResult> GetAllRoles(string selectedTenant)
        {
            var adminToken = IzendaTokenAuthorization.GetIzendaAdminToken();

            var izendaTenant = await IzendaUtilities.GetIzendaTenantByName(selectedTenant, adminToken);
            var roleDetailsByTenant = await IzendaUtilities.GetAllIzendaRoleByTenant(izendaTenant?.Id ?? null, adminToken);

            var roles = roleDetailsByTenant.Select(r => new { r.Id, r.Name }).ToList();
            SelectList _roleList = new SelectList(roles, "Id", "Name");

            return new JsonResult(_roleList);
        }
        #endregion
    }
}
