﻿using Microsoft.AspNetCore.Mvc;

namespace MVCCoreDM1.Areas.Main.Controllers
{
    [Area("Main")]
    [ApiExplorerSettings(IgnoreApi = true)]
    public class FallbackController : Controller
    {
        #region Methods
        public IActionResult Index()
        {
            // not supposed to be routed Index view
            return NotFound();
        }

        /// <summary>
        /// Render izenda BI view
        /// </summary>
        /// <returns>View</returns>
        [Route("izenda/report")]
        [Route("izenda/reportviewer")]
        [Route("izenda/reportviewerpopup")]
        [Route("izenda/dashboard")]
        [Route("izenda/settings")]
        [Route("izenda/new")]
        public IActionResult Izenda()
        {
            return View(); // TODO: figure out when this is hit
        } 
        #endregion
    }
}
