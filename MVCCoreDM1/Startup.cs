using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using MVCCoreDM1.Areas.Identity;
using MVCCoreDM1.DataAccess.Data;
using MVCCoreDM1.DataAccess.Repository;
using MVCCoreDM1.DataAccess.Repository.IRepository;
using MVCCoreDM1.Models;
using MVCCoreDM1.Utility;

namespace MVCCoreDM1
{
    public class Startup
    {
        #region Properties
        public IConfiguration Configuration { get; }
        #endregion

        #region CTOR
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        #endregion

        #region Methods
        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<ApplicationDbContext>(options =>
                options.UseSqlServer(
                    Configuration.GetConnectionString("DefaultConnection")));

            services.AddIdentity<ApplicationUser, IdentityRole>()
                .AddDefaultTokenProviders()
                .AddEntityFrameworkStores<ApplicationDbContext>();

            services.AddScoped<IUserClaimsPrincipalFactory<ApplicationUser>, CustomUserClaimsPrincipalFactory>();

            services.AddSingleton<IEmailSender, EmailSender>();

            // add UnitOfWork as a part of dependency injection
            services.AddScoped<IUnitOfWork, UnitOfWork>();

            services.AddControllersWithViews().AddRazorRuntimeCompilation();
            services.AddRazorPages();

            services.ConfigureApplicationCookie(options =>
            {
                options.LoginPath = $"/Identity/Account/Login";
                options.LogoutPath = $"/Identity/Account/Logout";
                options.AccessDeniedPath = $"/Identity/Account/AccessDenied";
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "API", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();

                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "API v1"));
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();
          
            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{area=Main}/{controller=Home}/{action=Index}/{id?}");

                endpoints.MapRazorPages();

                endpoints.MapControllerRoute(
                    name: "IframeViewer",
                    pattern: "{area=Main}/{controller=Home}/{action=IframeViewer}");

                // for export process required html image (e.g., pdf, gauge, ...)
                endpoints.MapControllerRoute(
                    name: "ReportPart",
                    pattern: "{area=Main}/{controller=Report}/{action=ReportPart}");

                // for sub report - new window and access from report viewer menu at nav bar
                endpoints.MapControllerRoute(
                    name: "ReportViewer",
                    pattern: "{area=Main}/{controller=Report}/{action=ReportViewer}");

                // for access from dashboard viewer menu at nav bar
                endpoints.MapControllerRoute(
                    name: "DashboardViewer",
                    pattern: "{area=Main}/{controller=Dashboard}/{action=DashboardViewer}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_reportviewer",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_report",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_reportviewerpopup",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_dashboard",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_settings",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");

                // Reroute
                endpoints.MapControllerRoute(
                    name: "izenda_new",
                    pattern: "{area=Main}/{controller=Fallback}/{action=Izenda}");
            });
        }
        #endregion
    }
}
